## Site Studio Master Template Selector

This module provides a field type and field widget for selecting Site Studio
master templates on entities. The widget allows you to select master
templates within Site Studio on content entities (nodes, users, etc) instead
of just the bundle template types.

This would allow you to set the master template for any given page itself.
It is useful for cases when you may want a particular content entity to
display in a different master template.

### Installation

1. Install the module.
2. Add the "Master template selector" field to a content entity.
3. Under Manage Form Display, set its widget to "Site Studio master template
   selector widget".
4. Create one or more master templates in Site Studio. Make sure they are
   "Enabled".
5. On the content edit screen, the template selector field will show master
   templates as options.

For every master template you add or edit, you will likely need to "Rebuild"
Site Studio to generate the actual template files behind the scenes with the
changes.

This module _may_ conflict with mixing this widget/field with the normal Site
Studio template selector field. If you have that template selector field in
use already there may be unforeseen issues combining it with this. Test it
out before running in production.
