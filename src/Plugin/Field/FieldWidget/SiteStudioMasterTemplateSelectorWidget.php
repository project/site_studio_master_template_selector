<?php

namespace Drupal\site_studio_master_template_selector\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'site_studio_master_template_selector_widget' field widget.
 */
#[FieldWidget(
  id: 'site_studio_master_template_selector_widget',
  label: new TranslatableMarkup('Site Studio master template selector widget'),
  field_types: [
    'site_studio_master_template_selector',
  ],
)]
class SiteStudioMasterTemplateSelectorWidget extends WidgetBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Get the entity this form is working on.
    if (method_exists($items, 'getParent')) {
      $entity = $items->getParent()->getValue();
    }
    else {
      return [];
    }

    // Get list of templates for this content type.
    $template_ids = $this->entityTypeManager->getStorage('cohesion_master_templates')->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', TRUE)
      ->condition('selectable', TRUE)
      ->sort('default', 'DESC')
      ->execute();

    $template_storage = $this->entityTypeManager->getStorage('cohesion_master_templates');
    $templates = $template_storage->loadMultiple($template_ids);

    $options = [
      '__default__' => t('Default master template'),
    ];

    foreach ($templates as $key => $template) {
      if ($template->status() || $items[$delta]->selected_template == $key) {
        $options[$key] = $this->t('%label', ['%label' => $template->get('label')]);
      }
    }

    $element['selected_template'] = [
      '#type' => 'select',
      '#title' => t('Select template:'),
      '#description' => t('Select a master template that will be used to wrap this content.'),
      '#options' => $options,
      '#default_value' => (isset($options[$items[$delta]->selected_template])) ? $items[$delta]->selected_template : '__default__',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$item) {
      if (isset($item['selected_template']['content'])) {
        $item['selected_template'] = $item['selected_template']['content'];
      }
    }
    return $values;
  }

}
