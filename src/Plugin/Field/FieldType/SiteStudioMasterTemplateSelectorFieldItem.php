<?php

namespace Drupal\site_studio_master_template_selector\Plugin\Field\FieldType;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of site_studio_master_template_selector.
 */
#[FieldType(
  id: "site_studio_master_template_selector",
  label: new TranslatableMarkup("Master template selector"),
  description: new TranslatableMarkup("Field to store a a selected master template."),
  category: "Site Studio",
  default_widget: "site_studio_master_template_selector_widget",
  default_formatter: "basic_string",
  cardinality: 1,
  module: "cohesion",
)]
class SiteStudioMasterTemplateSelectorFieldItem extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'selected_template' => [
          'type' => 'text',
          'size' => 'tiny',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('selected_template')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['selected_template'] = DataDefinition::create('string')->setLabel(t('Template'));
    return $properties;
  }

}
